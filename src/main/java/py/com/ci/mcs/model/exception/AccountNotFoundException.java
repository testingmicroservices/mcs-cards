package py.com.ci.mcs.model.exception;

public class AccountNotFoundException extends Exception {

    public AccountNotFoundException(String message, Throwable ex){
        super(message, ex);
    }

    public AccountNotFoundException(String message){
        super(message);
    }

}
