package py.com.ci.mcs.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExtendAccountCreditLinePost implements Serializable {

    @JsonProperty("account_number")
    private Long accountNumber;

    @JsonProperty("extension_amount")
    private Long extensionAmount;

}
