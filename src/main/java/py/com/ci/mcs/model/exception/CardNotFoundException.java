package py.com.ci.mcs.model.exception;

public class CardNotFoundException extends Exception {

    public CardNotFoundException(String message, Throwable ex){
        super(message, ex);
    }

    public CardNotFoundException(String message){
        super(message);
    }

}
