package py.com.ci.mcs.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExtendAccountCreditLinePostData implements Serializable {

    @JsonProperty("data")
    private ExtendAccountCreditLinePost data;

}
