package py.com.ci.mcs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.ci.mcs.entity.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
}
