package py.com.ci.mcs.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.ci.mcs.entity.Card;

import java.util.Optional;

@Repository
public interface CardRepository extends CrudRepository<Card, Long> {

    @Query("SELECT C FROM Card C WHERE C.cardNumber = :card_number")
    Optional<Card> findByCardNumber(@Param("card_number") Long cardNumber);

}
