package py.com.ci.mcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import py.com.ci.mcs.model.request.ExtendAccountCreditLinePostData;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGetData;
import py.com.ci.mcs.service.impl.AccountServiceImpl;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountServiceImpl accountService;

    @GetMapping("/{card_number}")
    public ResponseEntity findAccountLimit(
        @RequestHeader(value = "log_id", required = true) String logId,
        @PathVariable(value = "card_number", required = true) Long cardNumber
    ){
        try {
            AccountCreditLineInformationGet data = accountService.findAccountLine(logId, cardNumber);
            AccountCreditLineInformationGetData wrapper = new AccountCreditLineInformationGetData(data);
            return new ResponseEntity(wrapper, HttpStatus.OK);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @PostMapping(value = "/limit/extend", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity extendAccountLine(
            @RequestHeader(value = "log_id", required = true) String logId,
            @RequestBody ExtendAccountCreditLinePostData data
    ){
        try {
            accountService.extendAccountLine(logId, data.getData());
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
