package py.com.ci.mcs.service;

import py.com.ci.mcs.model.exception.AccountNotFoundException;
import py.com.ci.mcs.model.exception.CardNotFoundException;
import py.com.ci.mcs.model.request.ExtendAccountCreditLinePost;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;

public interface AccountService {

    AccountCreditLineInformationGet findAccountLine(String logId, Long cardNumber)
    throws CardNotFoundException, AccountNotFoundException;

    void extendAccountLine(String logId, ExtendAccountCreditLinePost data)
    throws AccountNotFoundException;

}
