package py.com.ci.mcs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ci.mcs.converter.AccountCreditLineConverter;
import py.com.ci.mcs.entity.Account;
import py.com.ci.mcs.entity.Card;
import py.com.ci.mcs.model.exception.AccountNotFoundException;
import py.com.ci.mcs.model.exception.CardNotFoundException;
import py.com.ci.mcs.model.request.ExtendAccountCreditLinePost;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;
import py.com.ci.mcs.model.util.CustomLog;
import py.com.ci.mcs.repository.AccountRepository;
import py.com.ci.mcs.repository.CardRepository;
import py.com.ci.mcs.service.AccountService;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LogManager.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private AccountCreditLineConverter converter;

    @Override
    public AccountCreditLineInformationGet findAccountLine(String logId, Long cardNumber)
    throws
    CardNotFoundException, AccountNotFoundException
    {
        LOGGER.info(new CustomLog(logId, "Finding account credit line information", cardNumber));
        Optional<Card> card = cardRepository.findByCardNumber(cardNumber);
        if(!card.isPresent()){
            LOGGER.error(new CustomLog(logId, "Card not exists", cardNumber));
            throw new CardNotFoundException("Card number: " + cardNumber + " not found");
        }
        Optional<Account> account = accountRepository.findById(card.get().getAccountNumber());
        if(!account.isPresent()){
            LOGGER.error(new CustomLog(logId, "Account not exists", card.get().getAccountNumber()));
            throw new AccountNotFoundException("Account " + card.get().getAccountNumber() + " not found");
        }
        return converter.entityToModel(card.get(), account.get());
    }

    @Override
    public void extendAccountLine(String logId, ExtendAccountCreditLinePost data)
    throws
    AccountNotFoundException
    {
        LOGGER.info(new CustomLog(logId, "Extending account line", data));
        Optional<Account> account = accountRepository.findById(data.getAccountNumber());
        if(!account.isPresent()){
            LOGGER.error(new CustomLog(logId, "Account not exists", data.getAccountNumber()));
            throw new AccountNotFoundException("Account " + data.getAccountNumber() + " not found");
        }
        Account accountInformation = account.get();
        accountInformation.extendCreditLine(data.getExtensionAmount());
        accountRepository.save(accountInformation);
    }

}
