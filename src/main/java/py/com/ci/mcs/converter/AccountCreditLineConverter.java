package py.com.ci.mcs.converter;

import org.springframework.stereotype.Component;
import py.com.ci.mcs.entity.Account;
import py.com.ci.mcs.entity.Card;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;

@Component
public class AccountCreditLineConverter {

    public AccountCreditLineInformationGet entityToModel(Card card, Account account){
        AccountCreditLineInformationGet model = new AccountCreditLineInformationGet();
        model.setCreditLine(account.getCreditLine());
        model.setPreapprovedAmount(account.getPreapprovedAmount());
        model.setFirstName(card.getFirstName());
        model.setLastName(card.getLastName());
        model.setAccountNumber(account.getAccountNumber());
        model.setCardNumber(card.getCardNumber());
        return model;
    }

}
