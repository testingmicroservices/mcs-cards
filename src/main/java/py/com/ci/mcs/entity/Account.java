package py.com.ci.mcs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "credit_line")
    private Long creditLine;

    @Column(name = "preapproved_amount")
    private Long preapprovedAmount;

    public void extendCreditLine(Long amountToExtend){
        creditLine = creditLine + amountToExtend;
        preapprovedAmount = preapprovedAmount - amountToExtend;
        if(preapprovedAmount < 0L){
            preapprovedAmount = 0L;
        }
    }

}
