package py.com.ci.mcs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cards")
public class Card implements Serializable {

    @Id
    @Column(name = "card_id")
    private Long cardId;

    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "card_number")
    private Long cardNumber;

    @Column(name = "holder_information")
    private String holderInformation;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

}
