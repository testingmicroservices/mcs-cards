package py.com.ci.mcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class McsCardsApplication {

	public static void main(String[] args) {
		SpringApplication.run(McsCardsApplication.class, args);
	}

}
