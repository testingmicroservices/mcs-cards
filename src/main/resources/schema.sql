DROP TABLE IF EXISTS accounts;
DROP TABLE IF EXISTS cards;

CREATE TABLE public.accounts (
    account_number IDENTITY NOT NULL,
    credit_line BIGINT NOT NULL,
    preapproved_amount BIGINT NOT NULL,
    CONSTRAINT accounts_pk PRIMARY KEY (account_number)
);

CREATE TABLE public.cards (
    card_id IDENTITY NOT NULL,
    account_number INTEGER NOT NULL,
    card_number BIGINT NOT NULL,
    holder_information VARCHAR(30) NOT NULL,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    CONSTRAINT cards_pk PRIMARY KEY (card_id)
);

ALTER TABLE public.cards ADD CONSTRAINT accounts_cards_fk
    FOREIGN KEY (account_number)
        REFERENCES public.accounts (account_number)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION;