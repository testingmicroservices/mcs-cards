INSERT INTO accounts
(account_number, credit_line, preapproved_amount)
VALUES (200154, 500, 0);

INSERT INTO cards
(account_number, card_number, holder_information, first_name, last_name)
VALUES (
   200154, 1005478980114879, 'ALBERTO ACOSTA', 'Alberto Jose', 'Acosta Sanchez'
);

INSERT INTO accounts
(account_number, credit_line, preapproved_amount)
VALUES (200879, 500, 100);

INSERT INTO cards
(account_number, card_number, holder_information, first_name, last_name)
VALUES (
   200879, 2307830000073373, 'FERNANDO GONZALEZ', 'Fernando', 'Gonzalez'
);
