package contracts
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Should find account limit from credit card 1"
    request {
        url value(
            consumer("/cards/account/1005478980114879"),
            producer("/account/1005478980114879")
        )
        headers {
            contentType(applicationJson())
            header 'log_id': 1
        }
        method GET()
    }
    response {
        status 200
        body('''
            {
                "data": {
                    "credit_line": 700,
                    "preapproved_amount": 0,
                    "first_name": "Alberto Jose",
                    "last_name": "Acosta Sanchez",
                    "account_number": 200154,
                    "card_number": 1005478980114879
                }
            }
            '''
        )
        headers {
            contentType(applicationJson())
        }
    }
}