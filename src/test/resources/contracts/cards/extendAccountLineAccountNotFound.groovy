package contracts
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Should extend credit line with account 1"
    request {
        url value(
            consumer("/cards/account/limit/extend"),
            producer("/account/limit/extend")
        )
        headers {
            contentType(applicationJson())
            header 'log_id': 1
        }
        method POST()
        body('''
            {
                "data": {
                    "account_number": 1,
                    "extension_amount": 200
                }
            }
            '''
        )
    }
    response {
        status 422
        body "Account 1 not found"
    }
}