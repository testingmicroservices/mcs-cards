package contracts
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Should find account limit from credit card 1"
    request {
        url value(
            consumer("/cards/account/1"),
            producer("/account/1")
        )
        urlPath""
        headers {
            contentType(applicationJson())
            header 'log_id': 1
        }
        method GET()
    }
    response {
        status 422
        body "Card number: 1 not found"
        headers {
            contentType(textPlain())
        }
    }
}